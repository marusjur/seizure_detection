import argparse
from SeizureDetection import SeizureDetection

def main(filename, random_seed):
    sd = SeizureDetection(random_seed=random_seed)
    sd.train()
    sd.save_model(filename)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train seizure detection model')
    parser.add_argument('filename', type=str, help='save model as filename')
    parser.add_argument('--random_seed', type=int, help='Random Seed', default=None, required=False)
    args = parser.parse_args()
    main(args.filename, args.random_seed)