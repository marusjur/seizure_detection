import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from SeizureDetection import SeizureDetection
from SeizureDetection import load_data, save_data, get_selections_indexes


def main(file, trained_model, sfreq, output, graph, test_labels_file):
    sd = SeizureDetection()
    sd.load_model(trained_model)
    # problem ak natrenovany model (pipeline transformer - downsample) pre inu nahravaciu frekvenciu
    if sfreq != sd.sfreq:
        print('--training new model--')
        sd = SeizureDetection(sfreq=sfreq)
        sd.train()
    data_raw = load_data(file)
    predictions = sd.seizure_detection(data_raw)
    data_raw[1] = pd.Series(predictions)
    save_data(data_raw, output)
    if graph:
        handles = []
        plt.plot(np.linspace(0, int(data_raw.shape[0] / sfreq), data_raw.shape[0]), data_raw[0], color='black')
        prediction_labels = data_raw[1].dropna()
        prediction_indexes, lengths = get_selections_indexes([prediction_labels.values])
        for i in range(len(prediction_indexes)):
            plt.axvspan(prediction_indexes[i] / sfreq, (prediction_indexes[i] / sfreq + lengths[i] / sfreq), alpha=0.5,
                        color='r')
        red_patch = mpatches.Patch(color='red', label='Seizure predictions', alpha=0.5)
        handles.append(red_patch)

        if test_labels_file:
            data = load_data(test_labels_file)
            true_labels, lengths = get_selections_indexes(data.values.T)
            for i in range(len(true_labels)):
                plt.axvspan(true_labels[i] / sfreq, (true_labels[i] / sfreq + lengths[i] / sfreq), alpha=0.5, color='g')
            green_patch = mpatches.Patch(color='green', label='Seizure labels', alpha=0.5)
            handles.append(green_patch)

        plt.legend(handles=handles)
        plt.xlabel('time [s]')
        filename = output.split('.csv')[0]
        plt.savefig('{}.png'.format(filename))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Detect seizure from eeg data in csv format')
    parser.add_argument('path', type=str, help='path to file with eeg data')
    parser.add_argument('trained_model', type=str, help='path to trained model')
    parser.add_argument('--output', type=str, help='path / filename of report', default='report.csv', required=False)
    parser.add_argument('--sfreq', type=float, help='Frequency of eeg recording', default=173.61, required=False)
    parser.add_argument('--graph', type=bool, help='generate graph', default=False, required=False)
    parser.add_argument('--labels', type=str, help='path to real labels (for generated test files)',
                        default=None, required=False)
    args = parser.parse_args()
    main(args.path, args.trained_model, args.sfreq, args.output, args.graph, args.labels)