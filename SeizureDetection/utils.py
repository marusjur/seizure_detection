import pandas as pd
import numpy as np

def load_data(path):
    """
    loads .csv or .txt data of EEG recording
    :param path: path to file containing EEG data
    :return: pandas Dataframe with data from file
    """
    if path.lower().endswith(('.csv', '.txt')):
        df = pd.read_csv(path, dtype=float, header=None, sep='\n')
        return df
    print('{} not csv'.format(path))

def save_data(df, path):
    """
    saves data as .csv file
    :param df: pandas dataframe
    :param path: path for saving data
    """
    df.to_csv(path, sep=',', index=False, header=False)

def get_selections_indexes(data, length=None):
    """
    get sections of seizures from labels type data
    :param data: np array of labels
    :param length: length of seizure (default None)
    :return: list of indexes, where seizure started; list of lengths of seizures
    """
    _, indexes_ = np.nonzero(data)
    if not length:
        prev = indexes_[0] - 1
        indexes = [prev]
        lengths = []
        l = 0
        for i in range(len(indexes_)):
            if indexes_[i] != prev + 1:
                indexes.append(indexes_[i])
                lengths.append(l)
                l = 0
            else:
                l += 1
            prev = indexes_[i]
        lengths.append(l)
    else:
        indexes = [indexes_[i * length] for i in range(int(len(indexes_) / length))]
        lengths = [length]*len(indexes)

    return indexes, lengths
