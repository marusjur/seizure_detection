import os
import wget
import zipfile
import random
import pandas as pd
import numpy as np
from .utils import load_data, save_data

class DatasetHandler:
    '''class for managing dataset and test data'''
    def __init__(self, random_seed=None):
        """
        init of DatasetHandler
        :param random_seed: random seed for all random features
        """
        self.DOWNLOAD_DIR = os.path.join('data', 'download')
        self.DOWNLOAD_URL = 'http://epileptologie-bonn.de/cms/upload/workgroup/lehnertz/'
        # SETS_URLS = [('setA', 'Z.zip'),('setB', 'O.zip'),('setC', 'N.zip'), ('setD', 'F.zip'), ('setE', 'S.zip')]
        self.SETS_URLS = [('setC', 'N.zip'), ('setD', 'F.zip'), ('setE', 'S.zip')]
        self.SEIZURE_SET = 'setE'
        self.DATASET_PATH = os.path.join('data', 'datasets')
        self.DATASET_SEIZURE_TEST = os.path.join(self.DATASET_PATH, 'test', 'seizure')
        self.DATASET_SEIZURE_TRAIN = os.path.join(self.DATASET_PATH, 'train', 'seizure')
        self.DATASET_NOT_SEIZURE_TEST = os.path.join(self.DATASET_PATH, 'test', 'not_seizure')
        self.DATASET_NOT_SEIZURE_TRAIN = os.path.join(self.DATASET_PATH, 'train', 'not_seizure')

        self.TEST_FILES_PATH = os.path.join('data', 'test_files')
        self.TEST_FILES_LABELS_PATH = os.path.join(self.TEST_FILES_PATH, 'labels')

        self.random_seed = random_seed
        if self.random_seed:
            random.seed(random_seed)

    def download_files(self):
        """
        downloads data from http://epileptologie-bonn.de/cms/upload/workgroup/lehnertz/
        """
        for set_name, set_url in self.SETS_URLS:
            if not os.path.exists(os.path.join(self.DOWNLOAD_DIR, set_name)):
                print('\ndownloading: {} - {}'.format(set_name, set_url))
                wget.download(os.path.join(self.DOWNLOAD_URL, set_url), self.DOWNLOAD_DIR)
                zip_ref = zipfile.ZipFile(os.path.join(self.DOWNLOAD_DIR, set_url), 'r')
                zip_ref.extractall(os.path.join(self.DOWNLOAD_DIR, set_name))
                zip_ref.close()
                os.remove(os.path.join(self.DOWNLOAD_DIR, set_url))

    def split_dataset(self, test_ratio=0.2):
        """
        splits downloaded data randomly to train & test directories based on test_ratio
        :param test_ratio: ratio for splitting downloaded data to train / test
        """
        for set_name in os.listdir(self.DOWNLOAD_DIR):
            files = os.listdir(os.path.join(self.DOWNLOAD_DIR, set_name))
            test_files = random.sample(files, int(len(files) * test_ratio))
            if set_name == self.SEIZURE_SET:
                test_path, train_path = self.DATASET_SEIZURE_TEST, self.DATASET_SEIZURE_TRAIN
            else:
                test_path, train_path = self.DATASET_NOT_SEIZURE_TEST, self.DATASET_NOT_SEIZURE_TRAIN
            for file in files:
                move_to_path = train_path
                if file in test_files:
                    move_to_path = test_path
                os.rename(os.path.join(self.DOWNLOAD_DIR, set_name, file), os.path.join(move_to_path, file))

    def generate_test_data(self, number_of_files=3, filename='test_file'):
        """
        generates .csv test files, with randomly mixed seizure & not seizure EEG recordings
        and generates .csv labels files with selections of seizures (for testing purposes)
        :param number_of_files: number of files to generate
        :param filename: base name of files, which will be created
        """
        not_seizure = []
        for file in [s for s in os.listdir(self.DATASET_NOT_SEIZURE_TEST) if s.lower().endswith('.txt')]:
            data = load_data(os.path.join(self.DATASET_NOT_SEIZURE_TEST, file))
            not_seizure.append(data)
        seizure = []
        for file in [s for s in os.listdir(self.DATASET_SEIZURE_TEST) if s.lower().endswith('.txt')]:
            data = load_data(os.path.join(self.DATASET_SEIZURE_TEST, file))
            seizure.append(data)
        if number_of_files > len(seizure):
            raise AssertionError("Maximum number of test files to generate is {}".format(len(seizure)))
        random.shuffle(seizure)
        random.shuffle(not_seizure)
        seizure = [seizure[i::number_of_files] for i in range(number_of_files)]
        not_seizure = [not_seizure[i::number_of_files] for i in range(number_of_files)]
        for i in range(number_of_files):
            file_sequences = seizure[i] + not_seizure[i]
            labels = [[1, len(seizure[i][j])] for j in range(len(seizure[i]))] + [[0, len(not_seizure[i][j])] for j in
                                                                                  range(len(not_seizure[i]))]
            zip_ = list(zip(file_sequences, labels))
            random.shuffle(zip_)
            file_sequences, labels = zip(*zip_)
            df = pd.concat(file_sequences)
            labels_ =np.array([[l[0]] * l[1] for l in list(labels)])
            labels = pd.DataFrame(np.concatenate(labels_))
            save_data(labels, os.path.join(self.TEST_FILES_LABELS_PATH, '{}_{}.csv'.format(filename, i)))
            save_data(df, os.path.join(self.TEST_FILES_PATH, '{}_{}.csv'.format(filename, i)))
            # daj bez csv - ? - user si zada sam ?!
