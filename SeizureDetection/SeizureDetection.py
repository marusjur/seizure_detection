import os
import joblib
import numpy as np
from .DatasetHandler import DatasetHandler
from .utils import load_data

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.pipeline import Pipeline

from mne_features.feature_extraction import FeatureExtractor
from .transforms import Downsample


class SeizureDetection:
    """class for managing data processing, training and seizure detection"""
    def __init__(self, sfreq=173.61 ,random_seed=None, dataset_handler=None):
        """
        init of SeizureDetection
        :param sfreq: recording frequency of
        :param random_seed: random seed for all random features
        :param dataset_handler: DatasetHandler object (if None -> will be created)
        """
        if not dataset_handler:
            self.datasethandler = DatasetHandler()
        else:
            self.datasethandler = dataset_handler
        self.random_seed = random_seed

        self.sfreq = sfreq
        self.train_sfreq = 173.61
        self.time_frame = 5
        self.file_length = 4097
        self.frame = int(self.train_sfreq * self.time_frame)
        self.frame_num = int(self.file_length / self.frame)

        self.selected_funcs = ['line_length']
        self.pipeline = Pipeline([
            ('dw', Downsample(num=self.frame)),
            ('fe', FeatureExtractor(sfreq=self.sfreq,
                                    selected_funcs=self. selected_funcs)),
            ('clf', RandomForestClassifier(n_estimators=100,
                                           max_depth=4,
                                           random_state=42))])
    def train(self):
        """
        load training data & train model
        """
        data_segments = []
        labels = []
        fnames_not_seizure = [s for s in os.listdir(self.datasethandler.DATASET_NOT_SEIZURE_TRAIN) if s.lower().endswith('.txt')]
        for fname in fnames_not_seizure:
            data_raw = load_data(os.path.join(self.datasethandler.DATASET_NOT_SEIZURE_TRAIN, fname))
            for g, df in data_raw.groupby(np.arange(len(data_raw)) // self.frame):
                if df.shape[0] == self.frame:
                    data_segments.append(df.values.reshape(1, 1, df.shape[0]))
        labels.append(np.zeros((len(fnames_not_seizure) * self.frame_num,)))

        fnames_seizure = [s for s in os.listdir(self.datasethandler.DATASET_SEIZURE_TRAIN) if s.lower().endswith('.txt')]
        for fname in fnames_seizure:
            _data = load_data(os.path.join(self.datasethandler.DATASET_SEIZURE_TRAIN, fname))
            for g, df in _data.groupby(np.arange(len(_data)) // self.frame):
                if df.shape[0] == self.frame:
                    data_segments.append(df.values.reshape(1, 1, df.shape[0]))
        labels.append(np.ones((len(fnames_seizure) * self.frame_num,)))

        data = np.concatenate(data_segments)
        y = np.concatenate(labels, axis=0)
        print('training on data with shape\ndata: {}\nlabels: {}'.format(data.shape, y.shape))

        skf = StratifiedKFold(n_splits=3, random_state=self.random_seed, shuffle=True)
        self.pipeline.fit(data, y)
        scores = cross_val_score(self.pipeline, data, y, cv=skf)
        print('Cross-validation accuracy score = %1.3f (+/- %1.5f)' % (np.mean(scores), np.std(scores)))

    def save_model(self, filename):
        """
        saves trained model as .pkl file
        :param filename: name / path for saving
        """
        joblib.dump(self.pipeline, '{}.pkl'.format(filename))

    def load_model(self, path):
        """
        loads saved .pkl model
        :param path: path to model
        """
        self.pipeline = joblib.load(path)

    def seizure_detection(self, data_raw):
        """
        detects epilepsy seizures in data
        :param data_raw: pandas dataframe of raw data
        :return: np array with predictions of seizure
        """
        predictions = []
        for g, df in data_raw.groupby(np.arange(len(data_raw)) // self.frame):
            if df.shape[0] == self.frame:
                data_segment = df.values.reshape(1, 1, df.shape[0])
                prediction = self.pipeline.predict(data_segment)
                if prediction[0] > 0.5:
                    predictions.append(np.ones(self.frame))
                else:
                    predictions.append(np.zeros(self.frame))
        predictions = np.concatenate(predictions, axis=0)
        return predictions
