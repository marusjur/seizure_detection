import numpy as np
from scipy.signal import resample
from sklearn.base import BaseEstimator, TransformerMixin

class Downsample(BaseEstimator, TransformerMixin):
    """"custom sklearn transformer used in pipeline"""
    def __init__(self, num=347):
        """
        init of sklearn Transformer
        :param num: length of data frame
        """
        self.num = num

    def transform(self, x):
        """
        downsample data to the same size as training data
        :param x: training data
        :return: downsampled data
        """
        data = []
        for i in range(x.shape[0]):
            data.append([resample(x[i][0], self.num)])

        return np.array(data)

    def fit(self, x, y):
        """
        does nothing (function needed for sklearn Transformer)
        :param x: data
        :param y: data labels
        :return: self
        """
        return self