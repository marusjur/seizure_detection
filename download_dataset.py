# download data from http://epileptologie-bonn.de/cms/front_content.php?idcat=193&lang=3&changelang=3
import os
import argparse
from SeizureDetection import DatasetHandler

def main(test_ratio, random_seed):
    dataset_handler = DatasetHandler(random_seed=random_seed)

    if not os.path.exists(dataset_handler.DOWNLOAD_DIR):
        os.makedirs(dataset_handler.DOWNLOAD_DIR)
        dataset_handler.download_files()

    downloaded_folders = os.listdir(dataset_handler.DOWNLOAD_DIR)
    if len(os.listdir(os.path.join(dataset_handler.DOWNLOAD_DIR, downloaded_folders[0]))) == 0:
        raise AssertionError("Dataset already downloaded.\nRemove download/ and datasets/ directories and try again !")

    for path in [dataset_handler.DATASET_SEIZURE_TEST, dataset_handler.DATASET_SEIZURE_TRAIN,
                 dataset_handler.DATASET_NOT_SEIZURE_TEST, dataset_handler.DATASET_NOT_SEIZURE_TRAIN]:
        if not os.path.exists(path):
            os.makedirs(path)

    dataset_handler.split_dataset(test_ratio=test_ratio)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download data and split dataset - test/train')
    parser.add_argument('--random_seed', type=int, help='Random Seed', default=None, required=False)
    parser.add_argument('--test_ratio', type=float, help='ration for dataset split', default=0.2, required=False)
    args = parser.parse_args()
    main(args.test_ratio, args.random_seed)