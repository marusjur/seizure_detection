import os
import argparse
from SeizureDetection import DatasetHandler

def main(random_seed, number_of_files, filename):
    dataset_handler = DatasetHandler(random_seed=random_seed)

    if not os.path.exists(dataset_handler.TEST_FILES_PATH):
        os.mkdir(dataset_handler.TEST_FILES_PATH)
    if not os.path.exists(dataset_handler.TEST_FILES_LABELS_PATH):
        os.mkdir(dataset_handler.TEST_FILES_LABELS_PATH)

    dataset_handler.generate_test_data(number_of_files=number_of_files, filename=filename)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate test csv files with seizure & not seizure data')
    parser.add_argument('--random_seed', type=int, help='Random Seed', default=None, required=False)
    parser.add_argument('--number_of_files', type=int, help='number of test files to generate', default=3, required=False)
    parser.add_argument('--filename', type=str, help='filename of test file', default='test_file', required=False)
    args = parser.parse_args()
    main(args.random_seed, args.number_of_files, args.filename)